package no.ntnu.idatg2001.model.persistency;


import no.ntnu.idatg2001.model.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test for Serializer class")
public class SerializerTest {


    @DisplayName("Test if an instance of serializer class is created correctly")
    @Test
    public void testConstructorWithValidInput() {
        List<Patient> testList = Arrays.asList(new Patient("Kari", "Larsen", "12345678901",
                "Allergi", "Dr.Olsen"), new Patient("Anne", "Larsen",
                "43215678901", "Allergi", "Dr.Olsen"));

        Serializer serializer = new Serializer(testList);

        assertEquals(testList.size(), serializer.getPatientsList().size());
    }

    @DisplayName("Test if constructor throws exception when null input")
    @Test
    public void testConstructorWithNullInput() {
        assertThrows(IllegalArgumentException.class, () -> new Serializer(null));
    }

    @DisplayName("Getter test")
    @Test
    public void testGetterOfSerializerList() {
        List<Patient> expected = new ArrayList<>();
        expected.add(new Patient("Kari", "Larsen", "12345678901",
                "Allergi", "Dr.Olsen"));
        List<Patient> actual = new ArrayList<>(new Serializer(expected).getPatientsList());

        assertEquals(expected, actual);
    }

    @Nested
    @DisplayName("Tests serialize method")
    public class SerializeMethodTest {

        private File notCSVFile;
        private File CSVFile;
        private Serializer serializer;

        @BeforeEach
        public void createCommonListToSerialize() {
            List<Patient> toBeSerialized = Arrays.asList(new Patient("Kari", "Larsen", "12345678901",
                    "Allergi", "Dr.Olsen"), new Patient("Anne", "Larsen",
                    "43215678901", "Allergi", "Dr.Olsen"));

            this.serializer = new Serializer(toBeSerialized);

        }

        @DisplayName("Test if null parameter is handled")
        @Test
        public void testIfNullParameterIsHandled() {
            assertThrows(IllegalArgumentException.class, () -> serializer.serialize(null));
        }

        @DisplayName("Test if not CSV input is handled")
        @Test
        public void testNoCSVInput() {
            try {
                notCSVFile = File.createTempFile("testPNG", ".png");

            } catch (IOException e) {
                fail("Failed to create temporary png file");
            }
            assertThrows(IllegalArgumentException.class, () -> serializer.serialize(notCSVFile));
        }

        @DisplayName("Test if CSV created correct serialized file")
        @Test
        public void testCSVInput(){
            try {
                CSVFile = File.createTempFile("testCSV", ".csv");

            } catch (IOException ioe) {
                fail("Failed to create temporary csv file");
            }

            List<String> expected = Arrays.asList("Kari;Larsen;12345678901;Allergi;Dr.Olsen",
                    "Anne;Larsen;43215678901;Allergi;Dr.Olsen");
            try {
                serializer.serialize(CSVFile);
                List<String> actual = Files.readAllLines(CSVFile.toPath());

                assertEquals(expected, actual);
            } catch (IOException ioe) {
                fail();
            }
        }
    }
}
