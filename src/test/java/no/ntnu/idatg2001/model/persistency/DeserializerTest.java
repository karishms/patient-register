package no.ntnu.idatg2001.model.persistency;

import no.ntnu.idatg2001.model.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test for Deserializer class")
public class DeserializerTest {

    private Deserializer deserializer;
    private File notCSVFile;
    private File CSVFile;

    @BeforeEach
    public void createCommonObject() {
        deserializer = new Deserializer();
    }

    @DisplayName("Test null input in deserializer throws exception")
    @Test
    public void testNullInput() {
        assertThrows(IllegalArgumentException.class, () -> deserializer.deserialize(null));
    }

    @DisplayName("Test if not CSV input is handled")
    @Test
    public void testNoCSVInput() {
        try {
            notCSVFile = File.createTempFile("testPNG", ".png");

        } catch (IOException e) {
            fail("Failed to create temporary png file");
        }
        assertThrows(IllegalArgumentException.class, () -> deserializer.deserialize(notCSVFile));
    }

    @DisplayName("Test if CSV is deserialized correctly")
    @Test
    public void testListIsDeserializedWhenCSVInput() {
        List<Patient> expected = Arrays.asList(new Patient("Kari", "Larsen", "12345678901",
                "Allergi", "Dr.Olsen"), new Patient("Anne", "Larsen",
                "43215678901", "Allergi", "Dr.Olsen"));

        try {
            CSVFile = File.createTempFile("testCSV", ".csv");

            //Writes objects to CSV file using Serializer class
            final List<Patient> objectsToWrite = Arrays.asList(new Patient("Kari", "Larsen", "12345678901",
                    "Allergi", "Dr.Olsen"), new Patient("Anne", "Larsen",
                    "43215678901", "Allergi", "Dr.Olsen"));

            new Serializer(objectsToWrite).serialize(CSVFile);

            List<Patient> actual = deserializer.deserialize(CSVFile);

            assertEquals(expected, actual);

        } catch (IOException e) {
            fail("Failed create CSV file for test");
        }

    }

}



