package no.ntnu.idatg2001.model;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName("Patient test")
public class PatientTest {


    @DisplayName("Tests that an instance of class Patient is correctly created using valid input")
    @Test
    public void constructorTestWithValidInput() {
        String socialSecurityNumber = "02030211223";
        String firstName = "Kari";
        String lastName = "Larsen";
        String diagnosis = "Allergi";
        String generalPractitioner = "Dr. Olsen";

        Patient patient = new Patient(firstName, lastName, socialSecurityNumber, diagnosis, generalPractitioner);

        assertEquals(socialSecurityNumber, patient.getSocialSecurityNumber());
        assertEquals(firstName, patient.getFirstName());
        assertEquals(lastName, patient.getLastName());
        assertEquals(diagnosis, patient.getDiagnosis());
        assertEquals(generalPractitioner, patient.getGeneralPractitioner());
    }

    @DisplayName("Accessor tests")
    @Nested
    public class AccessorTest {

        private Patient testPatient;
        private String socialSecurityNumber;
        private String firstName;
        private String lastName;
        private String diagnosis;
        private String generalPractitioner;


        @BeforeEach
        public void createCommonObject() {
            this.socialSecurityNumber = "02030211223";
            this.firstName = "Kari";
            this.lastName = "Larsen";
            this.diagnosis = "Allergi";
            this.generalPractitioner = "Dr. Olsen";

            this.testPatient = new Patient(firstName, lastName, socialSecurityNumber, diagnosis, generalPractitioner);
        }

        @DisplayName("Tests getter for social security number")
        @Test
        public void testIfActualSocialSecurityNumberIsReturned() {
            String expected = this.socialSecurityNumber;
            String actual = this.testPatient.getSocialSecurityNumber();

            assertEquals(expected, actual);
        }

        @DisplayName("Tests getter for first name")
        @Test
        public void testIfActualFirstNameIsReturned() {
            String expected = this.firstName;
            String actual = this.testPatient.getFirstName();

            assertEquals(expected, actual);
        }

        @DisplayName("Tests getter for last name")
        @Test
        public void testIfActualLastNameIsReturned() {
            String expected = this.lastName;
            String actual = this.testPatient.getLastName();

            assertEquals(expected, actual);

        }

        @DisplayName("Tests getter for diagnosis")
        @Test
        public void testIfActualDiagnosisIsReturned() {
            String expected = this.diagnosis;
            String actual = this.testPatient.getDiagnosis();

            assertEquals(expected, actual);

        }

        @DisplayName("Tests getter for general practitioner")
        @Test
        public void testIfActualGeneralPractitionerIsReturned() {
            String expected = this.generalPractitioner;
            String actual = this.testPatient.getGeneralPractitioner();

            assertEquals(expected, actual);
        }

    }

    @Nested
    @DisplayName("Mutators tests")
    public class MutatorTest {


        private String socialSecurityNumber;
        private String firstName;
        private String lastName;
        private String diagnosis;
        private String generalPractitioner;
        private Patient testPatient;

        @BeforeEach
        public void createCommonObject() {
            this.socialSecurityNumber = "02030211223";
            this.firstName = "Kari";
            this.lastName = "Larsen";
            this.diagnosis = "Allergi";
            this.generalPractitioner = "Dr. Olsen";

            this.testPatient = new Patient(firstName, lastName, socialSecurityNumber, diagnosis, generalPractitioner);
        }

        @DisplayName("Tests setter for first name with valid input")
        @Test
        public void testFirstNameSetterWithValidInput() {
            String firstName = "Lise";
            testPatient.setFirstName(firstName);

            assertEquals(firstName, testPatient.getFirstName());
        }

        @DisplayName("Tests setter for last name with valid input")
        @Test
        public void testLastNameSetterWithValidInput() {
            String lastname = "Johansen";
            testPatient.setLastName(lastname);

            assertEquals(lastname, testPatient.getLastName());
        }

        @DisplayName("Tests setter for social security number with valid input")
        @Test
        public void testSocialSecurityNumberSetterWithValidInput() {
            String socialSecurityNumber = "01234567890";
            testPatient.setSocialSecurityNumber(socialSecurityNumber);

            assertEquals(socialSecurityNumber, testPatient.getSocialSecurityNumber());
        }

        @DisplayName("Tests setter for diagnosis with valid input")
        @Test
        public void testDiagnosisSetterWithValidInput() {
            String diagnosis = "Diagnose";
            testPatient.setDiagnosis(diagnosis);

            assertEquals(diagnosis, testPatient.getDiagnosis());
        }

        @DisplayName("Tests setter for general practitioner with valid input")
        @Test
        public void testGeneralPractitionerSetterWithValidInput() {
            String generalPractitioner = "Johansen";
            testPatient.setGeneralPractitioner(generalPractitioner);

            assertEquals(generalPractitioner, testPatient.getGeneralPractitioner());
        }

        @DisplayName("Tests setter for first name with invalid input")
        @Test
        public void testFirstNameSetterWithInvalidInput() {
            String firstName = " ";

            assertThrows(IllegalArgumentException.class, () -> testPatient.setFirstName(firstName));
        }

        @DisplayName("Tests setter for last name with invalid input")
        @Test
        public void testLastNameSetterWithInvalidInput() {
            String lastName = " ";

            assertThrows(IllegalArgumentException.class, () -> testPatient.setLastName(lastName));
        }

        @DisplayName("Tests setter for social security number with invalid input")
        @Test
        public void testSocialSecurityNumberSetterWithInvalidInput() {
            String empty = " ";
            assertThrows(IllegalArgumentException.class, () -> testPatient.setSocialSecurityNumber(empty));
        }

        @DisplayName("Tests setter for diagnosis with invalid input")
        @Test
        public void testDiagnosisSetterWithInvalidInput() {
            String diagnosis = " ";

            assertThrows(IllegalArgumentException.class, () -> testPatient.setDiagnosis(diagnosis));
        }

        @DisplayName("Tests setter for general practitioner with invalid input")
        @Test
        public void testGeneralPractitionerSetterWithInvalidInput() {
            String generalPractitioner = " ";
            assertThrows(IllegalArgumentException.class, () -> testPatient.setGeneralPractitioner(generalPractitioner));
        }
    }
}
