package no.ntnu.idatg2001.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test for PatientRegisterClass")
public class PatientRegisterTest {

    @DisplayName("Tests that an instance of class PatientRegister is created correctly ")
    @Test
    public void testsIfConstructorCreatesEmptyRegister() {
        PatientRegister patientRegister = new PatientRegister();
        int sizeOfRegister = patientRegister.getPatients().size();

        assertEquals(0, sizeOfRegister);
    }

    @DisplayName("Tests if contructor throws exception when null parameter")
    @Test
    public void testConstructorWithNullParameter() {
        assertThrows(IllegalArgumentException.class, () -> new PatientRegister(null));
    }

    @Nested
    @DisplayName("Tests for addPatient()")
    public class AddPatientMethodTest {

        private PatientRegister patientRegister;
        private Patient testPatient;

        @BeforeEach
        public void createCommonObjects() {
            this.patientRegister = new PatientRegister();
            this.testPatient = new Patient("Kari", "Larsen", "12345678901", "Allergi", "Dr.Olsen");

        }

        @DisplayName("Test for addPatient with null parameter")
        @Test
        public void testAddPatientWithNullParameter() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.addPatient(null));
        }

        @DisplayName("Tests if method successfully adds non existing patient")
        @Test
        public void addNonExistingPatient() {
            assertTrue(patientRegister.addPatient(testPatient));
            assertTrue(patientRegister.getPatients().contains(testPatient));
        }


        @DisplayName("Tests if patient already existing in register is not added")
        @Test
        public void addAlreadyExistingPatient() {
            Patient patientToAdd = testPatient;
            patientRegister.addPatient(testPatient);
            assertFalse(patientRegister.addPatient(patientToAdd));
        }

    }

    @Nested
    @DisplayName("Test for removePatient()")
    public class removePatientTest {

        private PatientRegister patientRegister;
        private Patient testPatient;

        @BeforeEach
        public void createCommonObjects() {
            this.patientRegister = new PatientRegister();
            this.testPatient = new Patient("Kari", "Larsen", "12345678901", "Allergi", "Dr.Olsen");

            this.patientRegister.addPatient(testPatient);

        }


        @DisplayName("Test for removePatient with null parameter")
        @Test
        public void testAddPatientWithNullParameter() {
            assertThrows(IllegalArgumentException.class, () -> patientRegister.removePatient(null));
        }

        @DisplayName("Removing existing patient in register")
        @Test
        public void testRemoveExistingPatient() {
            assertTrue(patientRegister.removePatient(testPatient));
        }


        @DisplayName("Removing non existing patient in register")
        @Test
        public void testRemoveNonExistingPatient() {

            Patient patientToBeRemoved = new Patient();
            assertFalse(patientRegister.removePatient(patientToBeRemoved));
        }
    }
}
