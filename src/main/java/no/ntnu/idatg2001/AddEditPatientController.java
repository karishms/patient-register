package no.ntnu.idatg2001;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import no.ntnu.idatg2001.factory.alert.AlertFactory;
import no.ntnu.idatg2001.factory.alert.AlertType;
import no.ntnu.idatg2001.model.Patient;

import java.util.Optional;

/**
 * The type Add edit patient controller. For the window opening when the user chooses to add or edit
 * a patient
 */
public class AddEditPatientController {

    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField socialSecurityNumberTextField;
    @FXML
    private TextField diagnosisTextField;
    @FXML
    private TextField generalPractitionerTextField;

    private Patient patientToBeManaged = null;
    private Patient finalPatient = null;


    private boolean editing = false;

    private boolean emptyTextInput(TextField textfield) {
        return textfield.getText().trim().isEmpty();
    }

    private boolean emptyTextFields() {
        return emptyTextInput(this.firstNameTextField) || emptyTextInput(lastNameTextField) ||
                emptyTextInput(this.socialSecurityNumberTextField) || emptyTextInput(this.diagnosisTextField) || emptyTextInput(this.generalPractitionerTextField);
    }

    @FXML
    private void saveButtonClicked(ActionEvent actionEvent) {

        if (emptyTextFields()) {
            AlertFactory.create(AlertType.EMPTY_FIELDS).showAndWait();
            return;
        }

        if (this.patientHasUnsavedChanges()) {
            if (this.editing && !(this.verifiesNewChanges())) {
                return;
            }
        }

        this.finalPatient = this.getCurrentPatient();
        closeStage(actionEvent);

    }

    private boolean verifiesNewChanges() {
        Optional<ButtonType> result = AlertFactory.create(AlertType.VERIFY_NEW_CHANGES).showAndWait();
        return result.filter(buttonType -> buttonType == ButtonType.OK).isPresent();
    }


    /**
     * Gets final patient.
     *
     * @return the final patient
     */
    public Patient getFinalPatient() {
        return this.finalPatient;
    }

    private Patient getCurrentPatient() {

        return new Patient( this.firstNameTextField.getText(),
                this.lastNameTextField.getText(), this.socialSecurityNumberTextField.getText(),this.diagnosisTextField.getText(), this.generalPractitionerTextField.getText());

    }

    /**
     * Sets patient to edit.
     *
     * @param patientToBeEdited the patient to be edited
     */
    public void setPatientToEdit(Patient patientToBeEdited) {

        if (null != patientToBeEdited) {

            this.editing = true;

            this.firstNameTextField.setText(patientToBeEdited.getFirstName());
            this.lastNameTextField.setText(patientToBeEdited.getLastName());
            this.socialSecurityNumberTextField.setText(patientToBeEdited.getSocialSecurityNumber());
            this.diagnosisTextField.setText(patientToBeEdited.getDiagnosis());
            this.generalPractitionerTextField.setText(patientToBeEdited.getGeneralPractitioner());

            this.patientToBeManaged = patientToBeEdited;
        }

    }

    @FXML
    private void cancelButtonClicked(ActionEvent event) {

        if (!this.emptyTextFields()) {
            if (this.patientHasUnsavedChanges()) {
                if (!this.verifiesNewChanges()) {
                    return;
                }
            }
        }

        this.closeStage(event);

    }

    private boolean patientHasUnsavedChanges() {
        String socialSecurityNumber = this.socialSecurityNumberTextField.getText();
        String firstName = this.firstNameTextField.getText();
        String lastName = this.lastNameTextField.getText();
        String diagnosis = this.diagnosisTextField.getText();
        String generalPractitioner = this.generalPractitionerTextField.getText();

        Patient currentPatient = new Patient(firstName, lastName,socialSecurityNumber, diagnosis, generalPractitioner);

        return !(currentPatient.equals(this.patientToBeManaged));
    }

    private void closeStage(ActionEvent actionEvent) {
        Node source = (Node) actionEvent.getSource();
        Stage stage = (Stage) source.getScene().getWindow();

        stage.close();
    }

}
