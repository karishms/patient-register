package no.ntnu.idatg2001;


import java.io.File;
import java.io.IOException;
import java.net.URL;;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.ntnu.idatg2001.factory.StageFactory;
import no.ntnu.idatg2001.factory.alert.AlertFactory;
import no.ntnu.idatg2001.factory.alert.AlertType;
import no.ntnu.idatg2001.factory.SceneFactory;
import no.ntnu.idatg2001.model.Patient;
import no.ntnu.idatg2001.model.PatientRegister;
import no.ntnu.idatg2001.model.persistency.Deserializer;
import no.ntnu.idatg2001.model.persistency.Serializer;


/**
 * The type Main window controller.
 */
public class MainWindowController implements Initializable {

    private PatientRegister patientRegister;
    private static final String NAME = "Patient register";
    private static final String VERSION = "1.0-SNAPSHOT";

    @FXML
    private TableView<Patient> patientsTableView;
    @FXML
    private TableColumn<Patient, String> firstNameColumn;
    @FXML
    private TableColumn<Patient, String> lastNameColumn;
    @FXML
    private TableColumn<Patient, Integer> socialSecurityNumberColumn;
    @FXML
    private TableColumn<Patient, String> diagnosisColumn;
    @FXML
    private TableColumn<Patient, String> generalPractitionerColumn;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        patientRegister = new PatientRegister();
        this.patientsTableView.setEditable(true);
        this.patientsTableView.setItems(patientRegister.getPatients());
        setUpTableViewColumns();
    }

    private void setUpTableViewColumns() {
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        this.diagnosisColumn.setCellValueFactory(new PropertyValueFactory<>("diagnosis"));
        this.generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
    }

    private void clearSelection() {
        this.patientsTableView.getSelectionModel().clearSelection();
    }

    @FXML
    private void importCSVButtonClicked() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setTitle("Open Source File");

        File selectedFile = fileChooser.showOpenDialog(null);
        boolean overwriteConfirmed = true;

        if (null != selectedFile) {
            // If the table already has items, the user has to confirm to overwrite these
            if (patientsTableView.getItems().size() > 0) {
                overwriteConfirmed = overWriteConfirmed();
            }

            if (overwriteConfirmed) {
                Deserializer deserializer = new Deserializer();
                List<Patient> deserializedPatients = deserializer.deserialize(selectedFile);
                if (!deserializedPatients.isEmpty()) {
                    this.patientsTableView.getItems().clear();
                    this.patientsTableView.getItems().setAll(deserializedPatients);
                } else {
                    AlertFactory.create(AlertType.CANNOT_IMPORT).showAndWait();
                }
            }
        }
    }

    @FXML
    private void exportCSVButtonClicked() {
        //User wants to export an empty list of patients
        if(this.patientsTableView.getItems().size() == 0){
            AlertFactory.create(AlertType.EXPORT_EMPTY_LIST).showAndWait();
            return;
        }


        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV Files", "*.csv"));
        fileChooser.setTitle("Open Source File");

        File selectedFile = fileChooser.showSaveDialog(null);
        if (null != selectedFile) {
            Serializer serializer = new Serializer(patientsTableView.getItems());
            serializer.serialize(selectedFile);
        }
    }

    @FXML
    private void exitMenuItemClicked() {
        System.exit(0);
    }

    @FXML
    private void addPatient() throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AddPatientPopUp.fxml"));
        Parent root = fxmlLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setResizable(false);
        stage.setTitle("PATIENT DETAILS | Add patient ");
        stage.setScene(scene);
        stage.showAndWait();

        AddEditPatientController controller = fxmlLoader.getController();
        Patient patient = controller.getFinalPatient();

        if (patient != null) {
            if (patient.getSocialSecurityNumber().length() != 11) {
                AlertFactory.create(AlertType.MISSING_DIGITS).showAndWait();
                return;
            }

            if (!patientRegister.addPatient(patient)) {
                AlertFactory.create(AlertType.CANNOT_ADD).showAndWait();
                return;
            } else {
                this.patientsTableView.getItems().add(patient);
            }
        }
        this.clearSelection();
    }


    @FXML
    private void editPatient() throws IOException {

        if (!this.patientsTableView.getSelectionModel().isEmpty()) {
            Patient toEdit = this.patientsTableView.getSelectionModel().getSelectedItem();
            this.openPatientFormView(toEdit);
        } else
            AlertFactory.noSelectionAlert("edit").showAndWait();

        this.clearSelection();
    }

    private void openPatientFormView(Patient toEdit) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("AddPatientPopUp.fxml"));
        Parent parent = fxmlLoader.load();

        AddEditPatientController controller = fxmlLoader.getController();
        controller.setPatientToEdit(toEdit);

        Scene scene = SceneFactory.create(parent);
        Stage stage = StageFactory.create();
        stage.setResizable(false);
        stage.setTitle("Patient Details - Edit");
        stage.setScene(scene);
        stage.showAndWait();

        Patient patient = controller.getFinalPatient();

        if (patient != null) {
            if (patient.getSocialSecurityNumber().length() != 11) {
                AlertFactory.create(AlertType.MISSING_DIGITS).showAndWait();
            } else {
                int patientEditedIndex = this.patientsTableView.getItems().indexOf(toEdit);
                this.patientsTableView.getItems().set(patientEditedIndex, patient);
                this.clearSelection();
            }
        }
    }

    @FXML
    private void removePatient() {

        if (!this.patientsTableView.getSelectionModel().isEmpty()) {
            Patient toRemove = this.patientsTableView.getSelectionModel().getSelectedItem();
            if (this.removePatientConfirmed(toRemove)) {
                this.patientsTableView.getItems().remove(toRemove);
                this.patientRegister.removePatient(toRemove);
            }
        } else {
            AlertFactory.noSelectionAlert("delete").showAndWait();
        }

        this.clearSelection();
    }

    private boolean removePatientConfirmed(Patient patient) {
        Optional<ButtonType> result = AlertFactory.deleteConfirmationAlert(patient).showAndWait();
        return result.filter(buttonType -> buttonType == ButtonType.OK).isPresent();
    }

    @FXML
    private void aboutButtonClicked() {
        AlertFactory.informationDialog(NAME, VERSION).show();
    }

    private boolean overWriteConfirmed() {
        Optional<ButtonType> result = AlertFactory.create(AlertType.OVERWRITE_CSV).showAndWait();
        return result.filter(buttonType -> buttonType == ButtonType.OK).isPresent();
    }
}

