package no.ntnu.idatg2001.factory.node;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;

/**
 * The type Node factory.
 */
public class NodeFactory {

    private NodeFactory() {
    }

    /**
     * Create node.
     *
     * @param nodeType the node type
     * @return the node
     */
    public static Node create(NodeType nodeType){
        Node type;

        switch (nodeType){
            case H_BOX:
                type = new HBox();
                break;
            case MENUBAR:
                type = new MenuBar();
                break;
            case TOOLBAR:
                type = new ToolBar();
                break;
            case TABLEVIEW:
                type = new TableView<>();
                break;
            case TEXTFIELD:
                type = new TextField();
                break;
            case LABEL:
                type = new Label();
                break;
            default:
                throw new IllegalArgumentException(nodeType.name() + "is not defined.");

        }
        return type;
    }

}
