package no.ntnu.idatg2001.factory.node;

/**
 * The enum Node type.
 */
public enum NodeType {

    /**
     * Menubar node type.
     */
    MENUBAR,
    /**
     * Toolbar node type.
     */
    TOOLBAR,
    /**
     * Tableview node type.
     */
    TABLEVIEW,
    /**
     * H box node type.
     */
    H_BOX,
    /**
     * Textfield node type.
     */
    TEXTFIELD,
    /**
     * Label node type.
     */
    LABEL

}
