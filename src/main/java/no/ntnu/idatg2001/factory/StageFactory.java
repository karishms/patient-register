package no.ntnu.idatg2001.factory;

import javafx.stage.Stage;

/**
 * The type Stage factory.
 */
public class StageFactory {

    private StageFactory() {
    }

    /**
     * Create stage.
     *
     * @return the stage
     */
    public static Stage create() {
        return new Stage();
    }
}

