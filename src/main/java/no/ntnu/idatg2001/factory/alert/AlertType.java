package no.ntnu.idatg2001.factory.alert;

/**
 * The enum Alert type.
 */
public enum AlertType {

    /**
     * Cannot add alert type.
     */
    CANNOT_ADD,
    /**
     * Cannot edit alert type.
     */
    CANNOT_EDIT,
    /**
     * Empty fields alert type.
     */
    EMPTY_FIELDS,
    /**
     * Cannot delete alert type.
     */
    CANNOT_DELETE,
    /**
     * Verify new changes alert type.
     */
    VERIFY_NEW_CHANGES,
    /**
     * Cancel event alert type.
     */
    CANCEL_EVENT,
    /**
     * Missing digits alert type.
     */
    MISSING_DIGITS,
    /**
     * Overwrite CSV alert type.
     */
    OVERWRITE_CSV,

    /**
     * Cannot import alert type.
     */
    CANNOT_IMPORT,

    /**
     * Export empty list alert type.
     */
    EXPORT_EMPTY_LIST
}
