package no.ntnu.idatg2001.factory.alert;

import javafx.scene.control.Alert;
import no.ntnu.idatg2001.model.Patient;

/**
 * The type Alert factory.
 */
public class AlertFactory {

    private AlertFactory() {
    }

    /**
     * Create alert.
     *
     * @param inputType the input type
     * @return the alert
     */
    public static Alert create(AlertType inputType) {
        String header;
        String content;
        String title;
        Alert.AlertType type;

        switch (inputType) {
            case CANNOT_ADD:
                type = Alert.AlertType.ERROR;
                title = "Failed to add patient";
                header = "This patient already exists";
                content = "A patient with this exact social security number already exists.";
                break;
            case VERIFY_NEW_CHANGES:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm changes";
                header = "Confirming changes mean the loss the old one.";
                content = "You have made changes to the current patient. Are you sure?";
                break;
            case CANCEL_EVENT:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm cancel";
                header = "You are about to cancel.";
                content = "You might loose data doing so. Are you sure you want to cancel?";
                break;
            case EMPTY_FIELDS:
                type = Alert.AlertType.ERROR;
                title = "Cannot save";
                header = "One or more input fields are empty";
                content = "Cannot save changes while one or more input fields are empty.";
                break;
            case MISSING_DIGITS:
                type = Alert.AlertType.ERROR;
                title = "Cannot save the input patient";
                header = "The socialsecurity number has to be 11 digits.";
                content = "Please try again.";
                break;
            case OVERWRITE_CSV:
                type = Alert.AlertType.CONFIRMATION;
                title = "Confirm overwrite";
                header = "Your current data will be lost.";
                content = "If you do not export your current data, it will be lost" +
                        " \nwhen importing a new file! Are you sure?";
                break;
            case CANNOT_IMPORT:
                type = Alert.AlertType.INFORMATION;
                title = "Cannot import CSV";
                header = "There are some missing data in the CSV file";
                content = "Cannot import CSV file, because some of the data is missing. \nMake sure that " +
                        "all required attributes of a patient is present.";
                break;
            case EXPORT_EMPTY_LIST:
                type = Alert.AlertType.INFORMATION;
                title = "Export empty list";
                header = "There is no data in the application to export";
                content = "Cannot export CSV file because there is no data to export." +
                        "\n Please add patients or import an file containing necessary data";
                break;
            default:
                throw new IllegalArgumentException("UNKNOWN ERROR: " + inputType.name() + " is undefined.");
        }
        return createAlert(type, title, header, content);
    }


    private static Alert createAlert(Alert.AlertType alertType, String title, String headerText, String contentText) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        return alert;
    }

    /**
     * No selection alert alert.
     *
     * @param action the action
     * @return the alert
     */
    public static Alert noSelectionAlert(String action) {
        return createAlert(
                Alert.AlertType.ERROR,
                "No selection",
                "No patient was selected!",
                "To " + action.toLowerCase() + " a patient, one must first be selected."
        );
    }

    /**
     * Delete confirmation alert alert.
     *
     * @param patient the patient
     * @return the alert
     */
    public static Alert deleteConfirmationAlert(Patient patient) {

        return createAlert(
                Alert.AlertType.CONFIRMATION,
                "Delete confirmation",
                "You are about to delete " + patient.getFullName() + " - " + patient.getSocialSecurityNumber(),
                "Deleting this patient will permanently remove them and all saved information about them.\nAre you sure you would like to delete? \nClick OK to delete or Cancel to cancel."
        );
    }

    /**
     * Information dialog alert.
     *
     * @param name    the name
     * @param version the version
     * @return the alert
     */
    public static Alert informationDialog(String name, String version) {
        Alert info = new Alert(Alert.AlertType.INFORMATION);

        info.setTitle("Information Dialog - About");
        info.setHeaderText(name + "\n" + version);

        info.setContentText("A brilliant application created by " +
                "\n(C) Karishma Sharma \n 03.05.2021");
        return info;
    }

}
