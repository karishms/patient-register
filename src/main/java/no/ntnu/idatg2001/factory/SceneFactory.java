package no.ntnu.idatg2001.factory;

import javafx.scene.Parent;
import javafx.scene.Scene;

/**
 * The type Scene factory.
 */
public class SceneFactory {
    private SceneFactory() {
    }

    /**
     * Create scene.
     *
     * @param origin the origin
     * @return the scene
     */
    public static Scene create(Parent origin) {
        return new Scene(origin);
    }
}

