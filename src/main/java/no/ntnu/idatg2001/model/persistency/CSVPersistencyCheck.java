package no.ntnu.idatg2001.model.persistency;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

/**
 * The interface Csv persistency check.
 */
public interface CSVPersistencyCheck {

    /**
     * Checks if file is csv
     *
     * @param file the file
     * @return the boolean
     */
    default boolean checkIfFileIsCSV(File file) {
        if (null == file) {
            throw new IllegalArgumentException("Argument cannot be null.");
        }

        String extensionExpected = "csv";
        String actual = FilenameUtils.getExtension(file.getName());
        return actual.equalsIgnoreCase(extensionExpected);
    }
}
