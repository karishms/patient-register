package no.ntnu.idatg2001.model.persistency;

import no.ntnu.idatg2001.model.Patient;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The type Deserializer.
 */
public class Deserializer implements CSVPersistencyCheck {

    /**
     * Deserialize
     *
     * @param file the file
     * @return the list
     */
    public List<Patient> deserialize(File file) {

        List<Patient> deserializedPatients = new ArrayList<>();

        if (null == file) {
            throw new IllegalArgumentException("Argument cannot be null");
        }

        if (checkIfFileIsCSV(file)) {
            BufferedReader bufferedReader;
            try {
                bufferedReader = new BufferedReader(new FileReader(file));
                String fileColumns;

                while ((fileColumns = bufferedReader.readLine()) != null) {
                    String[] metadata = fileColumns.split(";");
                    if (metadata.length == 5) {
                        Patient patient = new Patient(metadata[0], metadata[1], metadata[2],
                                metadata[3], metadata[4]);
                        deserializedPatients.add(patient);
                    }
                }
                bufferedReader.close();
                return deserializedPatients;
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else
            throw new IllegalArgumentException("File is not CSV");

        return Collections.emptyList();
    }
}
