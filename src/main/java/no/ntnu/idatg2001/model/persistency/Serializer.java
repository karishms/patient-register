package no.ntnu.idatg2001.model.persistency;

import no.ntnu.idatg2001.model.Patient;

import java.io.*;
import java.util.List;

/**
 * The type Serializer.
 */
public class Serializer implements CSVPersistencyCheck {

    private final List<Patient> patientsList;

    /**
     * Instantiates a new Serializer.
     *
     * @param patients the patients
     */
    public Serializer(List<Patient> patients) {
        if (patients == null) {
            throw new IllegalArgumentException("Argument cannot be null");
        }
        this.patientsList = patients;
    }

    public List<Patient> getPatientsList() {
        return patientsList;
    }

    /**
     * Serializer.
     *
     * @param file the file
     */
    public void serialize(File file) {
        if (file == null) {
            throw new IllegalArgumentException("Argument cannot be null");
        }

        if (checkIfFileIsCSV(file)) {
            BufferedWriter bufferedWriter;
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(file));

                for (Patient patient : patientsList) {
                    String metadata = (patient.getFirstName() + ";" + patient.getLastName() + ";"
                            + patient.getSocialSecurityNumber() + ";" + patient.getDiagnosis() + ";"
                            + patient.getGeneralPractitioner());
                    try {
                        bufferedWriter.write(metadata);
                        bufferedWriter.newLine();
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
                bufferedWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }else
            throw new IllegalArgumentException("File has to be a CSV file");
    }

}
