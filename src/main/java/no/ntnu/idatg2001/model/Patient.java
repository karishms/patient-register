package no.ntnu.idatg2001.model;

import java.util.Objects;

/**
 * The type Patient.
 */
public class Patient {

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Instantiates a new Patient.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     * @param diagnosis            the diagnosis
     * @param generalPractitioner  the general practitioner
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber,
                   String diagnosis, String generalPractitioner) {
        setFirstName(firstName);
        setSocialSecurityNumber(socialSecurityNumber);
        setLastName(lastName);
        setDiagnosis(diagnosis);
        setGeneralPractitioner(generalPractitioner);
    }

    /**
     * Instantiates a new Patient.
     */
    public Patient() {
    }

    /**
     * Gets social security number.
     *
     * @return the social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }


    /**
     * Sets social security number.
     *
     * @param socialSecurityNumber the social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {

        this.socialSecurityNumber = checkIfStringParameterIsBlank(socialSecurityNumber);
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = checkIfStringParameterIsBlank(firstName);
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = checkIfStringParameterIsBlank(lastName);
    }

    /**
     * Gets full name.
     *
     * @return the full name
     */
    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * Sets diagnosis.
     *
     * @param diagnosis the diagnosis
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = checkIfStringParameterIsBlank(diagnosis);
    }

    /**
     * Gets general practitioner.
     *
     * @return the general practitioner
     */
    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets general practitioner.
     *
     * @param generalPractitioner the general practitioner
     */
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = checkIfStringParameterIsBlank(generalPractitioner);
    }

    private String checkIfStringParameterIsBlank(String toBeChecked) {
        if (toBeChecked.isBlank()) {
            throw new IllegalArgumentException("Parameter cannot be blank");
        } else return toBeChecked;
    }
    //chose to only have SSN because a patient can have the same name,diagnosis and doctor in theory.
    // Even though it is not likely
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", generalPractitioner='" + generalPractitioner + '\'' +
                '}';
    }
}
