package no.ntnu.idatg2001.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Iterator;
import java.util.List;

/**
 * The type Patient register.
 */
public class PatientRegister {

    private final ObservableList<Patient> patients;


    /**
     * Instantiates a new Patient register.
     */
    public PatientRegister() {
        this.patients = FXCollections.observableArrayList();
    }

    /**
     * Instantiates a new Patient register.
     *
     * @param patients the patients
     */
    public PatientRegister(List<Patient> patients) {
        if (null == patients) {
            throw new IllegalArgumentException("Argument cannot be null");
        }
        this.patients = FXCollections.observableArrayList(patients);
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public ObservableList<Patient> getPatients() {
        return FXCollections.observableArrayList(patients);
    }

    /**
     * Add patient to the register if not already existing.
     *
     * @param newPatient the new patient
     * @return the boolean
     */
    public boolean addPatient(Patient newPatient) {
        if (null == newPatient) {
            throw new IllegalArgumentException("Argument cannot be null");
        }

        boolean success;

        if (this.patients.contains(newPatient)) {
            success = false;
        } else {
            patients.add(newPatient);
            success = true;
        }
        return success;
    }

    /**
     * Removes given patient from the register
     *
     * @param patientToBeRemoved the patient to be removed
     * @return the boolean
     */
    public boolean removePatient(Patient patientToBeRemoved) {

        if (null == patientToBeRemoved) {
            throw new IllegalArgumentException("Argument cannot be null");
        }

        Iterator<Patient> it = patients.iterator();

        boolean success = false;
        while (it.hasNext()) {
            Patient obtainedPatient = it.next();
            if (obtainedPatient.equals(patientToBeRemoved)) {
                it.remove();
                success = true;
            }
        }

        return success;
    }


}

