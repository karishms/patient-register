module no.ntnu.idatg2001 {

    requires javafx.controls;
    requires javafx.fxml;
    requires org.apache.commons.io;

    opens no.ntnu.idatg2001 to javafx.fxml;
    exports no.ntnu.idatg2001;
    exports no.ntnu.idatg2001.model;
    opens no.ntnu.idatg2001.model to javafx.fxml;
    exports no.ntnu.idatg2001.factory;
    opens no.ntnu.idatg2001.factory to javafx.fxml;
    exports no.ntnu.idatg2001.factory.alert;
    opens no.ntnu.idatg2001.factory.alert to javafx.fxml;
    exports no.ntnu.idatg2001.factory.node;
    opens no.ntnu.idatg2001.factory.node to javafx.fxml;

}